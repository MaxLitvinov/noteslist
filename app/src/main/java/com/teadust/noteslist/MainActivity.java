package com.teadust.noteslist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.teadust.noteslist.adapters.NoteAdapter;
import com.teadust.noteslist.classes.Note;
import com.teadust.noteslist.classes.temp_package.RandomNote;
import com.teadust.noteslist.databases.NoteDatabase;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvNoteList;

    private List<com.teadust.noteslist.classes.Note> notes;

    private NoteDatabase database;

    private NoteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = new NoteDatabase(this);
        notes = database.getAllNotes();
        adapter = new NoteAdapter(this, notes);

        lvNoteList = (ListView) findViewById(R.id.lv_notes_list);
        lvNoteList.setAdapter(adapter);
        lvNoteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                id++;
                Intent intent = new Intent(MainActivity.this, NoteViewActivity.class);
                intent.putExtra("title", database.getNote(id).getTitle());
                intent.putExtra("short_description", database.getNote(id).getShortDescription());
                intent.putExtra("large_description", database.getNote(id).getLargeDescription());
                startActivity(intent);
            }
        });

        lvNoteList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                showDeleteDialog(id);
                return true;
            }
        });

        Button addNoteExample = (Button) findViewById(R.id.btn_add_note_example);
        addNoteExample.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Note note = RandomNote.getNote();
                database.addNote(note);
                notes.add(database.getLastAddedNote());
                print(notes);
                Log.d("111", "database.getNotesCount() " + String.valueOf(database.getNotesCount()));
            }
        });
    }

    private void print(List<com.teadust.noteslist.classes.Note> notes) {
        adapter = new NoteAdapter(this, notes);
        lvNoteList.setAdapter(adapter);
    }

    private void showDeleteDialog(final long id) {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(MainActivity.this);
        deleteDialog.setTitle("Deleting");
        deleteDialog.setMessage("Are you sure you want to delete this note?");
        deleteDialog.setIcon(android.R.drawable.ic_menu_delete);

        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                database.deleteNote(id);
            }
        });

        deleteDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        deleteDialog.show();
    }
}