package com.teadust.noteslist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.teadust.noteslist.R;
import com.teadust.noteslist.classes.Note;

import java.util.List;

public class NoteAdapter extends ArrayAdapter<Note> {

    public NoteAdapter(Context context, List<Note> notes) {
        super(context, R.layout.note_short_view, notes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Note note = getItem(position);
        NoteHolder noteHolder;
        if (convertView == null) {
            noteHolder = new NoteHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.note_short_view, parent, false);
            noteHolder.image = (ImageView) convertView.findViewById(R.id.imgv_note);
            noteHolder.title = (TextView) convertView.findViewById(R.id.tv_note_title);
            noteHolder.shortDescription = (TextView) convertView.findViewById(R.id.tv_note_short_description);
            convertView.setTag(noteHolder);
        } else {
            noteHolder = (NoteHolder) convertView.getTag();
        }
        noteHolder.title.setText(note.getTitle());
        noteHolder.shortDescription.setText(note.getShortDescription());
        return convertView;
    }

    private class NoteHolder {

        public ImageView image;
        public TextView title;
        public TextView shortDescription;
    }
}
