package com.teadust.noteslist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class NoteViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_view);

        Intent intent = getIntent();

        String title = intent.getStringExtra("title");
        String shortDescription = intent.getStringExtra("short_description");
        String largeDescription = intent.getStringExtra("large_description");

        TextView tvTitle = (TextView) findViewById(R.id.tv_title);
        TextView tvShortDescription = (TextView) findViewById(R.id.tv_short_description);
        TextView tvLargeDescription = (TextView) findViewById(R.id.tv_large_description);

        tvTitle.setText(title);
        tvShortDescription.setText(shortDescription);
        tvLargeDescription.setText(largeDescription);
    }
}
