package com.teadust.noteslist.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.teadust.noteslist.classes.Note;

import java.util.ArrayList;
import java.util.List;

public class NoteDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "StudentDatabase";

    private static class Column implements BaseColumns {
        public static final String TABLE_NAME = "table_note";
        public static final String ID = "id";
        public static final String TITLE = "title";
        public static final String SHORT_DESCRIPTION = "short_description";
        public static final String LARGE_DESCRIPTION = "large_description";
        public static final String[] ROW = { ID, TITLE, SHORT_DESCRIPTION, LARGE_DESCRIPTION };
    }

    private static class Query {
        public static final String CREATE_TABLE = "CREATE TABLE " + Column.TABLE_NAME
                + " (" + Column.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Column.TITLE + " TEXT, " + Column.SHORT_DESCRIPTION + " TEXT, "
                + Column.LARGE_DESCRIPTION + " TEXT);";
        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + CREATE_TABLE;
        public static final String GET_ALL_NOTES = "SELECT * FROM " + Column.TABLE_NAME;
    }

    public NoteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Query.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Query.DROP_TABLE);
    }

    public long addNote(Note note) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Column.TITLE, note.getTitle());
        values.put(Column.SHORT_DESCRIPTION, note.getShortDescription());
        values.put(Column.LARGE_DESCRIPTION, note.getLargeDescription());

        long addedNote = db.insert(Column.TABLE_NAME, null, values);

        db.close();

        return addedNote;
    }

    public void deleteNote(long id) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(Column.TABLE_NAME, Column.ID + "=?",
                new String[]{String.valueOf(id)});

        db.close();
    }

    public Note getNote(long id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(
                Column.TABLE_NAME,
                Column.ROW,
                Column.ID + "=?",
                new String[] { String.valueOf(id) },
                null, null, null, null);

        Note note = new Note();
        if (cursor != null) {
            cursor.moveToFirst();
            note.setId(Integer.parseInt(cursor.getString(0)));
            note.setTitle(cursor.getString(1));
            note.setShortDescription(cursor.getString(2));
            note.setLargeDescription(cursor.getString(3));
            cursor.close();
        }

        return note;
    }

    public Note getLastAddedNote() {
        long notesCount = getNotesCount();
        return getNote(notesCount);
    }

    public List<Note> getAllNotes() {
        List<Note> notesList = new ArrayList<Note>();

        String selectQuery = "SELECT * FROM " + Column.TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Note note = new Note();
                note.setId(c.getInt(c.getColumnIndex(Column.ID)));
                note.setTitle(c.getString(c.getColumnIndex(Column.TITLE)));
                note.setShortDescription(c.getString(c.getColumnIndex(Column.SHORT_DESCRIPTION)));
                note.setLargeDescription(c.getString(c.getColumnIndex(Column.LARGE_DESCRIPTION)));
                notesList.add(note);
            } while (c.moveToNext());
        }

        c.close();

        return notesList;
    }

    public long getNotesCount() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(Query.GET_ALL_NOTES, null);
        long notesCount = cursor.getCount();
        cursor.close();

        return notesCount;
    }
}
