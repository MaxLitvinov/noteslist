package com.teadust.noteslist.classes;

public class Note {

    private int id;
    private String title;
    private String shortDescription;
    private String largeDescription;

    public Note() {}

    public Note(int id, String title, String shortDescription, String largeDescription) {
        this.id = id;
        this.title = title;
        this.shortDescription = shortDescription;
        this.largeDescription = largeDescription;
    }

    public Note(String title, String shortDescription, String largeDescription) {
        this.title = title;
        this.shortDescription = shortDescription;
        this.largeDescription = largeDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLargeDescription() {
        return largeDescription;
    }

    public void setLargeDescription(String largeDescription) {
        this.largeDescription = largeDescription;
    }
}
