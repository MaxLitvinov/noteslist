package com.teadust.noteslist.classes.temp_package;

import com.teadust.noteslist.classes.Note;

public class RandomNote {

    private enum Title {

        MODE1 ("Red letter day"),
        MODE2 ("Walk the dog"),
        MODE3 ("Friday party"),
        MODE4 ("Make something wonderfull for Jane"),
        MODE5 ("Sam's Birthday");

        private String name;

        Title(String name) {
            this.name = name;
        }

        private static String getRandomValue() {
            return values()[(int) (Math.random() * values().length)].name;
        }
    }

    private enum ShortDescription {

        MODE1 ("Drink coffee in the \"Three Kings\""),
        MODE2 ("Ride on sled"),
        MODE3 ("Make snowman in the yard"),
        MODE4 ("Build timemachine"),
        MODE5 ("Bake a cake");

        private String name;

        ShortDescription(String name) {
            this.name = name;
        }

        private static String getRandomValue() {
            return values()[(int) (Math.random() * values().length)].name;
        }
    }

    private enum LargeDescription {

        MODE1 ("Coffee in \"Three Kings\" best buy after 15:00,\ncause it's Nancy changed."),
        MODE2 ("Alfred needs to be on the open air one hour per day."),
        MODE3 ("Buy for Sam the tablet device."),
        MODE4 ("Need some precision equipment for travelling."),
        MODE5 ("Take recipe from neighbor Jane");

        private String name;

        LargeDescription(String name) {
            this.name = name;
        }

        private static String getRandomValue() {
            return values()[(int) (Math.random() * values().length)].name;
        }
    }

    public static String getTitle() {
        return Title.getRandomValue();
    }

    public static String getShortDescription() {
        return ShortDescription.getRandomValue();
    }

    public static String getLargeDescription() {
        return LargeDescription.getRandomValue();
    }

    public static Note getNote() {
        return new Note(
                Title.getRandomValue(),
                ShortDescription.getRandomValue(),
                LargeDescription.getRandomValue()
        );
    }
}
